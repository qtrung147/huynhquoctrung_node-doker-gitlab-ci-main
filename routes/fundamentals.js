const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'Fundamentals'

router.get('/', function (req, res, next) {
    res.send({
        name: 'Fundamentals',
        server: 'express',
        variableData: variableData
    });
});
router.get('/sumTwoNumbers', function (req, res, next) {
    res.send({
        name: 'sumTwoNumbers',
        sum: sumTwoNumbers(1, 2)
    });
});
function sumTwoNumbers(a, b) {

    return a + b;
}
module.exports = router;
